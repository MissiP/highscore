const express = require('express')
const app = express();

app.use(express.json());

const scores = [{ name: 'Mike', score: 4 }, { name: 'Taylor', score: 3 }, { name: 'Sam', score: 2 }];


app.get('/scores', (req, res) => {
    res.status(200).send(scores);
});

app.post('/scores', (req, res) => {
    scores.push(req.body);
    scores.sort((score1, score2) => {
        //score 1: { name "", score: 0 }
        if (score1.score > score2.score) {
            return -1
        } else {
            return 1
        }
    })
    scores.length = 3;
    res.status(201).send();
});
app.set('port', 3000);
app.listen(app.get('port'), () => {
    console.log('Express server started on port', 3000);
});

